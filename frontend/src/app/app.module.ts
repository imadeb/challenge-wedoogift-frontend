import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { Level1Component } from './level1/level1.component';
import { Level2Component } from './level2/level2.component';

import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomHttpInterceptor } from './service/CustomHttpInterceptor';

@NgModule({
  declarations: [
    AppComponent,
    Level1Component,
    Level2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [{  provide: HTTP_INTERCEPTORS,
    useClass: CustomHttpInterceptor,
    multi: true
 }],
  bootstrap: [AppComponent]
})
export class AppModule { }
