import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CombinationOfCards } from '../model/CombinationOfCards';

@Component({
  selector: 'app-level2',
  templateUrl: './level2.component.html',
  styleUrls: ['./level2.component.css']
})
export class Level2Component implements OnInit {

  amount:number= 0;
  combinationOfCards:CombinationOfCards;
  msgError:string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.combinationOfCards = null;
    this.msgError = '';
  }

  nextAmount() { 
    this.msgError = '';
    if(this.combinationOfCards==null){
      this.findCombinationOfCards();
    }else{
      if(this.combinationOfCards.ceil!=null){
          this.amount++;
          console.log('nextAmount=> '+this.amount);
          if(this.amount && this.amount>0){
            this.getConbinationOfCards(this.amount)
            .subscribe(data => {
              console.log(data);
              this.combinationOfCards = data;
              if(this.combinationOfCards.ceil!=null){
                this.amount = +this.combinationOfCards.ceil.value;
              }else{
                this.amount--;
              }
            }, (error )=> { 
              console.error(error);
              this.msgError = 'Erreur technique lors de récupération des combinaisons de cartes !';
              this.combinationOfCards = null;
            });
          }
      }
    }
    console.log('nextAmount=> '+this.amount);
  }

  previousAmount(){
    this.msgError = '';
    if(this.combinationOfCards==null){
      this.findCombinationOfCards();
    }else{
      if(this.combinationOfCards.floor!=null){
          this.amount--;
          console.log('previousAmount=> '+this.amount);
          if(this.amount && this.amount>0){
            this.getConbinationOfCards(this.amount)
            .subscribe(data => {
              console.log(data);
              this.combinationOfCards = data;
              if(this.combinationOfCards.floor!=null){
                this.amount = +this.combinationOfCards.floor.value;
              }else{
                this.amount++;
              }
            }, (error )=> { 
              console.error(error);
              this.msgError = 'Erreur technique lors de récupération des combinaisons de cartes !';
              this.combinationOfCards = null;
            });
          }
      }
    }
    console.log('previousAmount=> '+this.amount);
  }

  findCombinationOfCards() { 
    this.msgError = '';
    try {
      if(this.amount && this.amount>0){
        this.getConbinationOfCards(this.amount)
        .subscribe(data => {
          console.log(data);
          this.combinationOfCards = data;
        }, (error )=> { 
          console.error(error);
          this.msgError = 'Erreur technique lors de récupération des combinaisons de cartes !';
          this.combinationOfCards = null;
        });
      }else{
        console.error('Please enter a correct amount !');
        this.msgError = 'Erreur de saisie du montant !';
        this.combinationOfCards = null;
      }
    } catch (error) {
      console.error(error);
      this.msgError = 'Erreur technique lors de récupération des combinaisons de cartes !';
      this.combinationOfCards = null;
    }
    
  }

  getConbinationOfCards(amount: number): Observable<any> {
    return this.http.get('http://localhost:3000/shop/5/search-combination?amount=' + amount) ;
  }

  onSelect(cardAmount: number): void {
    //this.amount = cardAmount;
  }

}

