export class Combination {
    value: number;
    cards: Array<number>;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}