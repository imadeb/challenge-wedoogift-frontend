import { Combination } from './Combination';

export class CombinationOfCards {
    equal: Combination;
    floor: Combination;
    ceil: Combination;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}